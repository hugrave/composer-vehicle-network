'use strict';
/**
 * Transactions logic for Vehicle network
 */

/**
 * Change owner transaction
 * @param {org.acme.vehicle.ChangeOwner} arg
 * @transaction
 */
function onChangeOwner (arg) {
    const namespace = "org.acme.vehicle"
    
    // Extracting argument values
    const vehicle = arg.vehicle;
    const newOwner = arg.newOwner;

    // Changing the owner
    vehicle.owner = newOwner;

    // Emit the event
    let event = getFactory().newEvent(namespace, 'OnChangeOwner');
    event.vehicle = vehicle;
    event.newOwner = newOwner;
    event.message = 'Vehichle ' + vehicle.getIdentifier() + ' has changed owner: ' + newOwner.getIdentifier();
    emit(event);

    // Update the registry
    return getAssetRegistry (namespace + '.Vehicle')
        .then ( assetRegistry => {
            return assetRegistry.update(vehicle);
        });
}

/**
 * Sell vehicle transaction
 * @param {org.acme.vehicle.SellVehicle} arg
 * @transaction
 */
function onSellVehicle (arg) {
    const namespace = 'org.acme.vehicle';

    // Extracting argument values
    let vehicleDetails = arg.vehicleDetails;
    const vin = arg.vin;
    const owner = arg.owner;
    const manufacturer = getCurrentParticipant();
    vehicleDetails.manufacturer = manufacturer;

    const factory = getFactory();
    // Creating the vehicle
    let vehicle = factory.newResource(namespace, 'Vehicle', vin);
    vehicle.vehicleDetails = vehicleDetails;
    vehicle.owner = owner;

    // Emit the event
    let event = factory.newEvent(namespace, 'OnSellVehicle');
    event.vehicle = vehicle;
    event.manufacturer = manufacturer;
    event.owner = owner;
    event.message = 'Manufacturer ' + manufacturer.getIdentifier() + ' sold ' + vehicle.getIdentifier() + ' to ' + owner.getIdentifier();
    emit(event);

    // Updating the registry
    return getAssetRegistry(namespace + '.Vehicle')
        .then ( assetRegistry => {
            return assetRegistry.add(vehicle);
        })
}